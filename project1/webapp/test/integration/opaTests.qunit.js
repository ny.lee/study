/* global QUnit */

sap.ui.require(["comistnsollinaproject01/project1/test/integration/AllJourneys"
], function () {
	QUnit.config.autostart = false;
	QUnit.start();
});
